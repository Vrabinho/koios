CREATE SCHEMA IF NOT EXISTS `koios` DEFAULT CHARACTER SET utf8;
USE `koios`;


CREATE TABLE IF NOT EXISTS `koios`.`members`
(
    `id`            INT          NOT NULL AUTO_INCREMENT,
    `firstName`     VARCHAR(30)  NOT NULL,
    `lastName`      VARCHAR(30)  NOT NULL,
    `address`       VARCHAR(255) NOT NULL,
    `city`          VARCHAR(80)  NOT NULL,
    `telephone`     VARCHAR(80)  NOT NULL,
    `birthDate`     DATE         NOT NULL,
    `membership_id` INT          NULL,
    PRIMARY KEY (`ID`),
    CONSTRAINT `membership_id__fk` FOREIGN KEY (`membership_id`) REFERENCES `members` (`id`)
)
    ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `koios`.`membership`
(
    `id`          INT          NOT NULL AUTO_INCREMENT,
    `amount`      DECIMAL      NOT NULL,
    `description` VARCHAR(300) NOT NULL,
    `isPaid`      TINYINT      NOT NULL,
    `datePaid`    DATE         NOT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB;