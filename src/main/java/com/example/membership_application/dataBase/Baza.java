package com.example.membership_application.dataBase;

import com.example.membership_application.model.Member;
import com.example.membership_application.model.Membership;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class Baza {

    public static boolean aktivnaVeza = false;


    private static synchronized Connection otvoriBazu() {
        aktivnaVeza = true;
        Connection veza = null;
        try {
            veza = DriverManager.getConnection("jdbc:mysql://localhost:3306/koios", "root", "Vrabinho1");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        Baza.aktivnaVeza = false;
        return veza;
    }

    private static void zatvoriBazu(Connection veza) {
        try {
            veza.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void addNewMember(Member newMember) throws SQLException {

        Connection veza = otvoriBazu();

        PreparedStatement upit = veza.prepareStatement("INSERT INTO members (firstName, lastName, address, city, telephone, birthDate) VALUES (?, ?, ?, ?, ?, ?)");

        upit.setString(1, newMember.getFirstName());
        upit.setString(2, newMember.getLastName());
        upit.setString(3, newMember.getAddress());
        upit.setString(4, newMember.getCity());
        upit.setString(5, newMember.getTelephone());
        upit.setDate(6, (java.sql.Date) newMember.getBirthDate());

        upit.executeUpdate();

        zatvoriBazu(veza);
    }

    public static void deleteMember(String firstAndLastName) throws SQLException {
        Connection veza = otvoriBazu();
        String[] str;
        String first;
        str = firstAndLastName.split(" ");
        first = str[0];

        PreparedStatement upit = veza.prepareStatement("DELETE FROM members WHERE firstname=(?)");
        upit.setString(1, first);
        upit.executeUpdate();
        zatvoriBazu(veza);
    }

    public static void deleteMembership(Long id) throws SQLException {
        Connection veza = otvoriBazu();

        PreparedStatement upit = veza.prepareStatement("UPDATE members SET membership_id = null WHERE membership_id=(?)");
        upit.setLong(1, id);
        upit.executeUpdate();
        PreparedStatement upit2 = veza.prepareStatement("DELETE FROM membership WHERE id=(?)");
        upit2.setLong(1, id);
        upit2.executeUpdate();
        zatvoriBazu(veza);
    }

    public static void addNewMembership(Membership newMembership) throws SQLException {

        Connection veza = otvoriBazu();

        PreparedStatement upit = veza.prepareStatement("INSERT INTO membership(amount, description, datePaid, isPaid) VALUES (?, ?, ?, ?)");
        upit.setBigDecimal(1, newMembership.getAmount());
        upit.setString(2, newMembership.getDescription());
        upit.setDate(3, (Date) newMembership.getDatePaid());
        upit.setBoolean(4, newMembership.getIsPaid());

        upit.executeUpdate();

        zatvoriBazu(veza);
    }

    public static void editMember(Member currentMember) throws SQLException {

        Connection veza = otvoriBazu();

        PreparedStatement upit = veza.prepareStatement("UPDATE members SET firstName=?, lastName=?, address=?, city=?, telephone=?, birthDate=? WHERE ID=" + currentMember.getId() + "");

        upit.setString(1, currentMember.getFirstName());
        upit.setString(2, currentMember.getLastName());
        upit.setString(3, currentMember.getAddress());
        upit.setString(4, currentMember.getCity());
        upit.setString(5, currentMember.getTelephone());
        upit.setDate(6, new Date(currentMember.getBirthDate().getTime()));

        upit.executeUpdate();

        zatvoriBazu(veza);
    }

    public static void editMembership(Membership currentMembership) throws SQLException {

        Connection veza = otvoriBazu();

        PreparedStatement upit = veza.prepareStatement("UPDATE membership SET amount=?, description=?, isPaid=?, datePaid=? WHERE ID=" + currentMembership.getId() + "");

        upit.setBigDecimal(1, currentMembership.getAmount());
        upit.setString(2, currentMembership.getDescription());
        upit.setBoolean(3, currentMembership.getIsPaid());
        upit.setDate(4, new Date(currentMembership.getDatePaid().getTime()));

        upit.executeUpdate();

        zatvoriBazu(veza);
    }

    public static void addingMembershipIntoMember(Long memberId, Long membershipId) throws SQLException {
        Connection veza = otvoriBazu();

        PreparedStatement upit = veza.prepareStatement("UPDATE members set membership_id=? where id=" + memberId + "");
        upit.setLong(1, membershipId);

        upit.executeUpdate();
        zatvoriBazu(veza);
    }

    public static void removeMember (Long member) throws SQLException {
        Connection veza = otvoriBazu();

        PreparedStatement upit = veza.prepareStatement("UPDATE members set membership_id=null where id =?");
        upit.setLong(1, member);
        upit.executeUpdate();
        zatvoriBazu(veza);
    }

    public static List<Membership> getAllMembership() throws SQLException, IOException {
        List<Membership> memberships = new ArrayList<>();

        Connection veza = otvoriBazu();

        Statement stmt = veza.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM membership");

        while (rs.next()) {
            java.lang.Long id = rs.getLong("id");
            BigDecimal amount = rs.getBigDecimal("amount");
            String description = rs.getString("description");
            boolean isPaid = rs.getBoolean("isPaid");
            Date datePaid = rs.getDate("datePaid");
            List<Member> memberId = getMemberMembership(new Membership(id, amount, description, datePaid, isPaid));

            Membership newMembership = new Membership(id, amount, description, datePaid, isPaid);
            newMembership.setListOfMembers(memberId);
            memberships.add(newMembership);
        }

        zatvoriBazu(veza);
        return memberships;
    }

    public static List<Member> getMemberMembership(Membership m1) throws SQLException, IOException {
        List<Member> members = new ArrayList<>();
        Connection veza = otvoriBazu();
        Member newMember = null;
        Statement stmt = veza.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM members where membership_id = " + m1.getId() + "");

        while (rs.next()) {
            java.lang.Long id = rs.getLong("id");
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            String address = rs.getString("address");
            String city = rs.getString("city");
            String telephone = rs.getString("telephone");
            Date dateOfBirth = rs.getDate("birthDate");

            newMember = new Member(id, firstName, lastName, address, city, telephone, dateOfBirth);
            members.add(newMember);
        }
        return members;
    }

    public static List<Member> getAllMembers() throws SQLException, IOException {
        List<Member> members = new ArrayList<>();

        Connection veza = otvoriBazu();

        Statement stmt = veza.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM members");

        while (rs.next()) {
            java.lang.Long id = rs.getLong("id");
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            String address = rs.getString("address");
            String city = rs.getString("city");
            String telephone = rs.getString("telephone");
            Date dateOfBirth = rs.getDate("birthDate");

            Member newMember = new Member(id, firstName, lastName, address, city, telephone, dateOfBirth);
            members.add(newMember);
        }

        zatvoriBazu(veza);
        return members;
    }
}
