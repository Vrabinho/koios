package com.example.membership_application.controllers;

import com.example.membership_application.dataBase.Baza;
import com.example.membership_application.model.Member;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

import java.sql.SQLException;

public class ShowMemberController {

    @FXML
    private Label firstAndLastName;

    @FXML
    private Label addressOfMember;

    @FXML
    private Label cityOfMember;

    @FXML
    private Label telephoneOfMember;

    @FXML
    private Label birthDateOfMember;


    public void showMember(Member member) {
        this.firstAndLastName.setText(member.getFirstName() + " " + member.getLastName());
        this.addressOfMember.setText(member.getAddress());
        this.cityOfMember.setText(member.getCity());
        this.telephoneOfMember.setText(member.getTelephone());
        this.birthDateOfMember.setText(member.getBirthDate().toString());
    }

    public void deleteSelectedMember() throws SQLException {
        Baza.deleteMember(this.firstAndLastName.getText());
        Alert deleted = new Alert(Alert.AlertType.INFORMATION);
        deleted.setTitle("Deleting member ");
        deleted.setHeaderText("Sucesfully deleted member");
        deleted.setContentText("Member sucesfully deleted from database.");
        deleted.showAndWait();
    }
}
