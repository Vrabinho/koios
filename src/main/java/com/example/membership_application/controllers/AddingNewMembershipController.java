package com.example.membership_application.controllers;

import com.example.membership_application.dataBase.Baza;
import com.example.membership_application.model.Member;
import com.example.membership_application.model.Membership;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class AddingNewMembershipController implements Initializable {

    public Button add;
    List<Member> members = new ArrayList<>();
    List<Membership> memberships = new ArrayList<>();

    @FXML
    private TextField amount;

    @FXML
    private TextField description;

    @FXML
    private TextField isPaid;

    @FXML
    private DatePicker datePaid;

    @FXML
    private ChoiceBox<String> members_id;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            memberships.addAll(Baza.getAllMembership());
            members.addAll(Baza.getAllMembers());

        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }

        for (Member member : members) {
            members_id.getItems().add(member.getFirstName());
        }
        members_id.setValue("Select members:");
    }

    public Member getAllMembers(ChoiceBox<String> members_id, List<Member> members) {
        String member_name = members_id.getValue();
        Member newMember = null;

        for (Member member : members) {
            if (member_name.equals(member.getFirstName())) {
                newMember = member;
            }
        }
        return newMember;
    }

    public java.lang.Long getMembershipId(List<Membership> memberships) {
        Membership lastMembership = memberships.get(memberships.size() - 1);

        return lastMembership.getId() + 1;
    }

    public void addMembership() throws ParseException, SQLException {
        Long idMembershipWrite;
        try {
            idMembershipWrite = getMembershipId(memberships);
        } catch (IndexOutOfBoundsException e) {
            idMembershipWrite = null;
        }
        BigDecimal amountWriter = new BigDecimal(amount.getText());
        String descriptionWriter = description.getText();
        boolean isPaidWriter = isPaid.getText().contains("1");
        LocalDate datePaidWriter = datePaid.getValue();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-dd-MM");
        String datePaidWriteString = datePaidWriter.format(formatter);
        java.util.Date dPWriter = new SimpleDateFormat("yyyy-dd-MM").parse(datePaidWriteString);
        java.sql.Date datePaidDatabase = new java.sql.Date(dPWriter.getTime());

        Membership newMembership = new Membership(idMembershipWrite, amountWriter, descriptionWriter, datePaidDatabase, isPaidWriter);

        Baza.addNewMembership(newMembership);
        Alert membership = new Alert(Alert.AlertType.INFORMATION);
        membership.setTitle("Adding new membership");
        membership.setHeaderText("Sucesfully added new membership");
        membership.setContentText("Membership " + idMembershipWrite + " sucesfully added to membership.");
        membership.showAndWait();
    }

    public void addMembersMembership() throws SQLException {
        Long idMembershipWrite = getMembershipId(memberships);
        Long membersId;
        try {
            membersId = getAllMembers(members_id, members).getId();
        } catch (IndexOutOfBoundsException e) {
            membersId = null;
        }
        Baza.addingMembershipIntoMember(membersId, idMembershipWrite);
        Alert membersMembership = new Alert(Alert.AlertType.INFORMATION);
        membersMembership.setTitle("Adding new member into membership");
        membersMembership.setHeaderText("Sucesfully added new member");
        membersMembership.setContentText("Member " + getAllMembers(members_id, members).getFirstName() + " " + getAllMembers(members_id, members).getLastName() + " sucesfully added to membership.");
        membersMembership.showAndWait();
    }

}
