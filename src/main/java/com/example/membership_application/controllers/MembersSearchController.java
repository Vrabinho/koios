package com.example.membership_application.controllers;

import com.example.membership_application.Main;
import com.example.membership_application.dataBase.Baza;
import com.example.membership_application.model.Member;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MembersSearchController implements Initializable {

    private static ObservableList<Member> observableListMembers;
    private static final ObservableList<Member> filteredObservableListOfMembers = FXCollections.observableArrayList();
    Boolean firstStart = true;

    @FXML
    List<Member> members = new ArrayList<>();

    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private TableView<Member> tableOfMembers;

    @FXML
    private TableColumn<Member, String> columnFirstName;

    @FXML
    private TableColumn<Member, String> columnLastName;

    @FXML
    private TableColumn<Member, String> columnAddress;

    @FXML
    private TableColumn<Member, String> columnCity;

    @FXML
    private TableColumn<Member, String> columnTelephone;

    @FXML
    private TableColumn<Member, Date> columnDoB;


    public void initialize(URL url, ResourceBundle resourceBundle) {

        columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
        columnCity.setCellValueFactory(new PropertyValueFactory<>("city"));
        columnTelephone.setCellValueFactory(new PropertyValueFactory<>("telephone"));
        columnDoB.setCellValueFactory(new PropertyValueFactory<>("birthDate"));

        if (observableListMembers == null) {
            observableListMembers = FXCollections.observableArrayList();
        }
        if (firstStart) {
            observableListMembers.clear();
            try {
                members = Baza.getAllMembers();
            } catch (SQLException | IOException throwables) {
                throwables.printStackTrace();
            }
            observableListMembers.addAll(members);
            tableOfMembers.setItems(observableListMembers);
            firstStart = false;
        }

        tableOfMembers.setRowFactory(tableOfMembers -> {
            TableRow<Member> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Member member = row.getItem();
                    doubleClick(member);
                } else if (event.isSecondaryButtonDown() && (!row.isEmpty())) {
                    Member member = row.getItem();
                    rightClick(member);
                }
            });
            return row;
        });
    }

    @FXML
    public void searchMember() {
        String name = firstName.getText();
        String surname = lastName.getText();

        List<Member> filteredListOfMembers = observableListMembers.stream()
                .filter(member -> member.getFirstName().toLowerCase().contains(name))
                .filter(member -> member.getLastName().toLowerCase().contains(surname))
                .collect(Collectors.toList());

        filteredObservableListOfMembers.clear();
        filteredObservableListOfMembers.addAll(FXCollections.observableArrayList(filteredListOfMembers));
        tableOfMembers.setItems(filteredObservableListOfMembers);
    }

    public void doubleClick(Member member) {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("showMember.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            assert false;
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();

            ShowMemberController controller = fxmlLoader.getController();
            controller.showMember(member);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void rightClick(Member member) {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("editingMember.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            assert false;
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();

            EditingMemberController controller = fxmlLoader.getController();
            controller.editMember(member);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
