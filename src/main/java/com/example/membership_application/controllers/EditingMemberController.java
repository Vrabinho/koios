package com.example.membership_application.controllers;

import com.example.membership_application.dataBase.Baza;
import com.example.membership_application.model.Member;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class EditingMemberController {

    Member currentMember;
    Member editedMember;
    @FXML
    private TextField firstNameEdit;

    @FXML
    private TextField lastNameEdit;

    @FXML
    private TextField addressEdit;

    @FXML
    private TextField cityEdit;

    @FXML
    private TextField telephoneNumberEdit;

    @FXML
    private TextField dateOfBirthEdit;


    public void editMember(Member member) {

        currentMember = member;
        firstNameEdit.setText(member.getFirstName());
        lastNameEdit.setText(member.getLastName());
        addressEdit.setText(member.getAddress());
        cityEdit.setText(member.getCity());
        telephoneNumberEdit.setText(member.getTelephone());
        dateOfBirthEdit.setText(member.getBirthDate().toString());

    }

    public void editMemberDatabase() throws SQLException, ParseException {
        java.lang.Long idmemberWrite = currentMember.getId();
        String firstNameWriter = firstNameEdit.getText();
        String lastNameWriter = lastNameEdit.getText();
        String addressWriter = addressEdit.getText();
        String cityWriter = cityEdit.getText();
        String telephoneWriter = telephoneNumberEdit.getText();
        Date dateOfBirthWriter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(dateOfBirthEdit.getText());

        editedMember = new Member(idmemberWrite, firstNameWriter, lastNameWriter, addressWriter, cityWriter, telephoneWriter,
                dateOfBirthWriter);
        Baza.editMember(editedMember);
        Alert updated = new Alert(Alert.AlertType.INFORMATION);
        updated.setTitle("Editing current member ");
        updated.setHeaderText("Sucesfully edited member");
        updated.setContentText("Member sucesfully edited and added to database.");
        updated.showAndWait();
    }
}
