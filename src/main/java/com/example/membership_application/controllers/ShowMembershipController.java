package com.example.membership_application.controllers;

import com.example.membership_application.dataBase.Baza;
import com.example.membership_application.model.Member;
import com.example.membership_application.model.Membership;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ShowMembershipController {

    @FXML
    private Label idMembership;

    @FXML
    private Label amountMembership;

    @FXML
    private Label descriptionMembership;

    @FXML
    private Label isPaidMembership;

    @FXML
    private Label datePaidMembership;

    @FXML
    private ListView<Member> membersIdMembership;

    Long id;

    public void showMembership(Membership membership) throws SQLException, IOException {
        List<Member> members = Baza.getMemberMembership(membership);
        id = membership.getId();
        idMembership.setText(membership.getId().toString());
        amountMembership.setText(membership.getAmount().toString());
        descriptionMembership.setText(membership.getDescription());
        if (membership.getIsPaid()) {
            isPaidMembership.setText("Paid");
        } else
            isPaidMembership.setText("Not Paid");
        datePaidMembership.setText(membership.getDatePaid().toString());
        ObservableList<Member> observableList = FXCollections.observableArrayList(membership.getListOfMembers());
        ObservableList<Member> observableList2 = FXCollections.observableArrayList();
        for (Member member : observableList) {
            for (Member member1 : members) {
                if (member.getId().equals(member1.getId())) {
                    observableList2.add(member);

                }
            }
        }
        membersIdMembership.setItems(observableList2);
    }


    public void deleteSelectedMembership() throws SQLException {
        Baza.deleteMembership(id);
        Alert deleted = new Alert(Alert.AlertType.INFORMATION);
        deleted.setTitle("Deleting membership ");
        deleted.setHeaderText("Sucesfully deleted membership");
        deleted.setContentText("Membership sucesfully deleted from database.");
        deleted.showAndWait();
    }
}
