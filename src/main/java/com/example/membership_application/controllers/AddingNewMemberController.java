package com.example.membership_application.controllers;

import com.example.membership_application.dataBase.Baza;
import com.example.membership_application.model.Member;
import com.example.membership_application.model.Membership;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class AddingNewMemberController implements Initializable {

    List<Member> members = new ArrayList<>();
    List<Membership> memberships = new ArrayList<>();

    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private TextField address;

    @FXML
    private TextField city;

    @FXML
    private TextField telephoneNumber;

    @FXML
    private DatePicker dateOfBirth;

    @FXML
    ListView<Member> memberListView = new ListView<>();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            memberships.addAll(Baza.getAllMembership());
            members.addAll(Baza.getAllMembers());

        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }

        memberListView.getItems().addAll(members);
        memberListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    }

    public java.lang.Long getMemberId(List<Member> member) {
        Member lastMember = member.get(member.size() - 1);

        return lastMember.getId() + 1;
    }

    public void addMember() throws ParseException, SQLException {
        java.lang.Long idmemberWrite;
        try{
            idmemberWrite = getMemberId(members);
        }catch (IndexOutOfBoundsException e) {
            idmemberWrite = null;
        }
        String firstNameWriter = firstName.getText();
        String lastNameWriter = lastName.getText();
        String addressWriter = address.getText();
        String cityWriter = city.getText();
        String telephoneWriter = telephoneNumber.getText();
        LocalDate dateOfBirthWriter = dateOfBirth.getValue();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-dd-MM");
        String dateOfBirthWriteString = dateOfBirthWriter.format(formatter);
        java.util.Date dobWriter = new SimpleDateFormat("yyyy-dd-MM").parse(dateOfBirthWriteString);
        java.sql.Date dateOfBirthDatabase = new java.sql.Date(dobWriter.getTime());

        Member newMember = new Member(idmemberWrite, firstNameWriter, lastNameWriter, addressWriter, cityWriter, telephoneWriter,
                dateOfBirthDatabase);

        Baza.addNewMember(newMember);
        Alert member = new Alert(Alert.AlertType.INFORMATION);
        member.setTitle("Adding new member");
        member.setHeaderText("Sucesfully added new member");
        member.setContentText("Member " + firstNameWriter + " " + lastNameWriter + " sucesfully added to database.");
        member.showAndWait();

    }
}
