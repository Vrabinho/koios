package com.example.membership_application.controllers;

import com.example.membership_application.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class StartScreenController {
    @FXML
    public Button membersSearch;

    @FXML
    public Button addingNewMember;

    @FXML
    public void searchAllMembers() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("membersSearch.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 900, 400);
        assert false;
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void addingNewMember() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("addingNewMember.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        assert false;
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void addingNewMembership() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("addingNewMembership.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        assert false;
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void searchAllMembership() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("membershipSearch.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 900, 400);
        assert false;
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }
}