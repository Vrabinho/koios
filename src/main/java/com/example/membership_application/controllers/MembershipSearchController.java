package com.example.membership_application.controllers;

import com.example.membership_application.Main;
import com.example.membership_application.dataBase.Baza;
import com.example.membership_application.model.Member;
import com.example.membership_application.model.Membership;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class MembershipSearchController implements Initializable {

    private static ObservableList<Membership> observableListOfMemberships;
    Boolean firstStart = true;

    @FXML
    List<Membership> memberships = new ArrayList<>();


    @FXML
    private TableView<Membership> tableOfMemberships;

    @FXML
    private TableColumn<Membership, String> columnId;

    @FXML
    private TableColumn<Membership, String> columnAmount;

    @FXML
    private TableColumn<Membership, String> columnDescription;

    @FXML
    private TableColumn<Membership, String> columnIsPaid;

    @FXML
    private TableColumn<Membership, Date> columnDatePaid;

    @FXML
    private TableColumn<Membership, Member> columnMembers;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        columnDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        columnIsPaid.setCellValueFactory(new PropertyValueFactory<>("isPaid"));
        columnDatePaid.setCellValueFactory(new PropertyValueFactory<>("datePaid"));

        if (observableListOfMemberships == null) {
            observableListOfMemberships = FXCollections.observableArrayList();
        }
        if (firstStart) {
            observableListOfMemberships.clear();
            try {
                memberships = Baza.getAllMembership();
            } catch (SQLException | IOException throwables) {
                throwables.printStackTrace();
            }
            observableListOfMemberships.addAll(memberships);
            tableOfMemberships.setItems(observableListOfMemberships);
            firstStart = false;
        }

        tableOfMemberships.setRowFactory(tableOfMembers -> {
            TableRow<Membership> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Membership membership = row.getItem();
                    doubleClick(membership);
                } else if (event.isSecondaryButtonDown() && (!row.isEmpty())) {
                    Membership membership = row.getItem();
                    rightClick(membership);
                }
            });
            return row;
        });
    }

    public void doubleClick(Membership membership) {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("showMembership.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            assert false;
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();

            ShowMembershipController controller = fxmlLoader.getController();
            controller.showMembership(membership);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void rightClick(Membership membership) {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("editMembership.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            assert false;
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();

            EditMembershipController controller = fxmlLoader.getController();
            controller.editMembership(membership);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
