package com.example.membership_application.controllers;

import com.example.membership_application.dataBase.Baza;
import com.example.membership_application.model.Member;
import com.example.membership_application.model.Membership;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class EditMembershipController extends StringConverter<BigDecimal> implements Initializable {

    List<Membership> memberships = new ArrayList<>();
    List<Member> members = new ArrayList<>();
    List<Long> membersIdWriter = null;

    Membership currentMembership;
    public Button add;

    public Button remove;

    @FXML
    private TextField amountEdit;

    @FXML
    private TextField descriptionEdit;

    @FXML
    private TextField isPaidEdit;

    @FXML
    private TextField datePaidEdit;

    @FXML
    private ChoiceBox<String> membersIdEdit;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            members.addAll(Baza.getAllMembers());

        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }

        for (Member member : members) {
            membersIdEdit.getItems().add(member.getFirstName());
        }
        membersIdEdit.setValue(membersIdEdit.getId());
    }

    public Member getAllMembers(ChoiceBox<String> members_id, List<Member> members) {
        String member_name = members_id.getValue();
        Member newMember = null;

        for (Member member : members) {
            if (member_name.equals(member.getFirstName())) {
                newMember = member;
            }
        }
        return newMember;
    }

    public java.lang.Long getMembershipId(List<Membership> memberships) {
        Membership lastMembership = memberships.get(memberships.size() - 1);

        return lastMembership.getId() + 1;
    }


    public void editMembership(Membership membership) {

        currentMembership = membership;
        amountEdit.setText(membership.getAmount().toString());
        descriptionEdit.setText(membership.getDescription());
        if (membership.getIsPaid()) {
            isPaidEdit.setText("Paid");
        } else
            isPaidEdit.setText("Not paid");
        datePaidEdit.setText(membership.getDatePaid().toString());
        membersIdEdit.setValue(membership.getListOfMembers().toString());

    }

    public void editMembershipDatabase() throws ParseException, SQLException {
        java.lang.Long idWrite = currentMembership.getId();
        BigDecimal amountWriter = fromString(amountEdit.getText());
        String descriptionwriter = descriptionEdit.getText();
        boolean isPaidWriter = isPaidEdit.getText().contains("1");
        Date datePaidWriter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(datePaidEdit.getText());
        membersIdWriter = null;
        for (Membership m1 : memberships) {
            assert false;
            membersIdWriter.add(m1.getId());
        }

        currentMembership = new Membership(idWrite, amountWriter, descriptionwriter, datePaidWriter, isPaidWriter);

        Baza.editMembership(currentMembership);
        Alert updated = new Alert(Alert.AlertType.INFORMATION);
        updated.setTitle("Editing current membership ");
        updated.setHeaderText("Sucesfully edited membership");
        updated.setContentText("Membership sucesfully edited and added to database.");
        updated.showAndWait();
    }

    public void removeMember() throws SQLException {
        Long membersId;
        try {
            membersId = getAllMembers(membersIdEdit, members).getId();
        } catch (IndexOutOfBoundsException e) {
            membersId = null;
        }
        Baza.removeMember(membersId);
        Alert membersMembership = new Alert(Alert.AlertType.INFORMATION);
        membersMembership.setTitle("Removing member from membership");
        membersMembership.setHeaderText("Sucesfully removed member from membership");
        membersMembership.setContentText("Member " + getAllMembers(membersIdEdit, members).getFirstName() + " " + getAllMembers(membersIdEdit, members).getLastName() + " sucesfully removed from membership.");
        membersMembership.showAndWait();
    }

    public void addMembersMembership() throws SQLException {
        Long idMembershipWrite = currentMembership.getId();
        Long membersId;
        try {
            membersId = getAllMembers(membersIdEdit, members).getId();
        } catch (IndexOutOfBoundsException e) {
            membersId = null;
        }
        Baza.addingMembershipIntoMember(membersId, idMembershipWrite);
        Alert membersMembership = new Alert(Alert.AlertType.INFORMATION);
        membersMembership.setTitle("Adding member into membership");
        membersMembership.setHeaderText("Sucesfully added new member");
        membersMembership.setContentText("Member " + getAllMembers(membersIdEdit, members).getFirstName() + " " + getAllMembers(membersIdEdit, members).getLastName() + " sucesfully added to membership.");
        membersMembership.showAndWait();
    }

    @Override
    public String toString(BigDecimal bigDecimal) {
        return null;
    }

    @Override
    public BigDecimal fromString(String s) {
        return BigDecimal.valueOf(Long.parseLong(s));
    }
}
