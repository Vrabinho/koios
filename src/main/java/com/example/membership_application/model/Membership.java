package com.example.membership_application.model;

import java.math.BigDecimal;
import java.util.*;

public class Membership {

    public Long id;
    public BigDecimal amount;
    public String description;
    public Date datePaid;
    public boolean isPaid;
    public List<Member> listOfMembers = new ArrayList<>();

    public Membership(java.lang.Long id, BigDecimal amount, String description, Date datePaid, boolean isPaid) {
        this.id = id;
        this.amount = amount;
        this.description = description;
        this.datePaid = datePaid;
        this.isPaid = isPaid;
    }

    public java.lang.Long getId() {
        return id;
    }

    public void setId(java.lang.Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Date datePaid) {
        this.datePaid = datePaid;
    }

    public boolean getIsPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public List<Member> getListOfMembers() {
        return listOfMembers;
    }

    public void setListOfMembers(List<Member> listOfMembers) {
        this.listOfMembers = listOfMembers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Membership)) return false;
        Membership that = (Membership) o;
        return getIsPaid() == that.getIsPaid() && Objects.equals(getId(), that.getId()) && Objects.equals(getAmount(), that.getAmount()) && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getDatePaid(), that.getDatePaid()) && Objects.equals(getListOfMembers(), that.getListOfMembers());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAmount(), getDescription(), getDatePaid(), getIsPaid(), getListOfMembers());
    }


}
