package com.example.membership_application.model;

import java.util.Date;
import java.util.Objects;

public class Member {

    java.lang.Long id;
    String firstName;
    String lastName;
    String address;
    String city;
    String telephone;
    Date birthDate;

    public Member(java.lang.Long id, String firstName, String lastName, String address, String city, String telephone, Date birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.telephone = telephone;
        this.birthDate = birthDate;
    }

    public java.lang.Long getId() {
        return id;
    }

    public void setId(java.lang.Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member)) return false;
        Member member = (Member) o;
        return Objects.equals(getId(), member.getId()) && Objects.equals(getFirstName(), member.getFirstName()) && Objects.equals(getLastName(), member.getLastName()) && Objects.equals(getAddress(), member.getAddress()) && Objects.equals(getCity(), member.getCity()) && Objects.equals(getTelephone(), member.getTelephone()) && Objects.equals(getBirthDate(), member.getBirthDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getAddress(), getCity(), getTelephone(), getBirthDate());
    }

    @Override
    public String toString() {
        return "" + firstName +
                " " + lastName +
                ", " + address +
                ", " + city +
                ", " + telephone +
                ", " + birthDate + ".";
    }
}
