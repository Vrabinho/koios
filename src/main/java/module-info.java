module com.example.membership_application {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.example.membership_application to javafx.fxml;
    exports com.example.membership_application.controllers;
    opens com.example.membership_application.controllers to javafx.fxml;
    exports com.example.membership_application;
    opens com.example.membership_application.model to javafx.fxml;
    exports com.example.membership_application.model;
}